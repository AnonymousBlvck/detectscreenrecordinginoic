var exec = require('cordova/exec');

module.exports.isRecording = function (success, error) {
    exec(success, error, 'DetectScreenRecording', 'isRecording', []);
};
